// directory that contains files that is used to build an application.
// reverse domain name notation
package com.zuitt;


import javax.sound.midi.Soundbank;
import java.sql.SQLOutput;
import java.util.Locale;

//"Main" is the entry point of a java program and is responsible for executing our code.
public class Main {
    public static void main(String[] args){
//        System.out.println("Hello World");
        /*
            [SECTION] Variables
            - Syntax:
                Variable Declaration
                    datatype identifier;
                Variable Declaration and Initialization
                    dataType identifier = value;
        */
        int myNum;

        int myNum2 = 30;
        System.out.println("Result of variable declaration and initialization:");
        System.out.println(myNum2);

        final int PRINCIPAL = 1000;

        /*
        [SECTION] Primitive Data Type
        -use to store simple values
         */

        // Single quotes (char)
        char letter = 'A';
        boolean isMarried =  false;

        // -128 to 127
        byte students = 127;

        //-32768 to 32767
        short seats = 32767;

        // Underscores may be placed in between number for code readability
        int localPopulation = 2_147_483_647;

        long worldPopulation = 7_862_881_145L;

        // [SECTION] Float and Doubles
        // The difference between using float and double depends on the preciseness of the values.
        float price = 12.99F;
        double temperature = 15869.8623941;

        // Checking the date type of identifier/variable.
        System.out.println("Result of getClass Method:");
        System.out.println(((Object)temperature).getClass());

        // [SECTION] Non-primitive
        String name = "John Doe";
        System.out.println("Result of Non-primitive Data Type:");
        System.out.println(name);
        String editName = name.toLowerCase();
        System.out.println(editName);

        //[SECTION] Type Casting

        // Implicit Casting
        int num1 = 5;
        double num2 = 2.7;
        double total = num1 + num2;
        System.out.println("Result from implicit casting:");
        System.out.println(total);

        // Explicit Casting
        int num3 = 5;
        double num4 = 2.7;
        int anotherTotal = num3 + (int)num4;
        System.out.println("Result from explicit casting:");
        System.out.println(anotherTotal);
    }
}
